import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import CategoryFood from './component/CategoryFood';

export default function App() {
  return (
    <View style={styles.container}>
      <CategoryFood/>
      <CategoryFood/>
      <CategoryFood/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'stretch',
    justifyContent: 'center',
    paddingLeft: 16,
    paddingRight: 16,
  },
});
