import React from 'react'
import {
    Image,
    View,
    Text,
    StyleSheet,
} from 'react-native'
import DogImage from '../assets/dog.png';

export default function CategoryFood (props){
    return <View style={styles.container}>
        <Text style={styles.title}>This is dog!</Text>
        <Image style={styles.categoryImage} source={DogImage}></Image>
    </View>
}

const styles = StyleSheet.create({
    container: {
        elevation: 1,
        alignItems:'center',
        padding: 16,
        borderRadius: 4,
        backgroundColor: '#FFF',
        shadowColor: '#000',
        shadowOpacity: 0.3,
        shadowRadius: 10,
        shadowOffset: { width: 0, height:0 },
        marginBottom: 16,
    },
    title: {
        textTransform: 'uppercase',
        marginBottom: 8,
        fontWeight: '700'
    },
    categoryImage: {
        width:64,
        height:64
    },

});